<?php
return [
    'adminEmail'     => 'admin@example.com',
    'icon-framework' => \kartik\icons\Icon::FAS, // Font Awesome Icon framework
    'mercadopago' => [
        'dev' => [
            'payer' => [
                "public_key"        => "APP_USR-de464b0a-c37c-4fa9-8d52-807689de74c1",
                "access_token"      => "APP_USR-7241411842144917-070401-830371f981fe3157b7ce5b8e432c5faa-785505553",
                "test_public_key"   => "TEST-c7a1a25c-ad0f-4ce3-a91a-700ef4b0b725",
                "test_access_token" => "TEST-7241411842144917-070401-8eb729d650a34e92c1e0e35411644939-785505553",
                "client_id"         => "7241411842144917",
                "client_secret"     => "5NdTz03xGoEqfkkdIANGpkT8ZEZbTXLC",
            ],
            'seller' => [
                "public_key"        => "APP_USR-3a26eb2d-1c9c-4c2d-b557-4f107696f6a0",
                "access_token"      => "APP_USR-689313783802280-070401-fe7a05ce310f2f6dc13bd431a2cd4b54-785506287",
                "test_public_key"   => "TEST-819b6f37-0abe-4a7a-b8eb-e9849f0d5054",
                "test_access_token" => "TEST-689313783802280-070401-148c848f178f575543c3232dadf0660c-785506287",
                "client_id"         => "689313783802280",
                "client_secret"     => "G5BJIauj1LmBNER6dXK0XuOMxYud6c9K",
            ]
            /*'payer' => [
                "public_key"        => "APP_USR-4801837f-ff66-4e4a-9f23-728a34bdde30",
                "access_token"      => "APP_USR-5160367441155481-070621-bc1c3f1f1f58db160b6f5cdef5afdbe5-787002062",
                "test_public_key"   => "TEST-c4a6de11-805d-46bc-93cd-cb86d731bb81",
                "test_access_token" => "TEST-5160367441155481-070621-61fd0225e09bd5ee44d65d6302af3d12-787002062",
                "client_id"         => "5160367441155481",
                "client_secret"     => "VWOV5T2NIlokBI6d68peaLbhhS6kXak7",
            ],
            'seller' => [
                "public_key"        => "APP_USR-bbdce30d-d8f2-49b5-b88d-9de4c0bb3010",
                "access_token"      => "APP_USR-7499799747585369-070621-c2909592cd1d486f10310e8d91749c3c-787009329",
                "test_public_key"   => "TEST-3c97cd91-a852-4ee5-b570-f8064e01c05e",
                "test_access_token" => "TEST-7499799747585369-070621-3d726b53676eee10dfedcd60e5b047c4-787009329",
                "client_id"         => "7499799747585369",
                "client_secret"     => "a9XOMvQFU8Icidzl9sk0NzVGrG1GKoja",
            ]*/
        ],
        'prod' => [
            "public_key"        => "APP_USR-0f07bf5a-823d-4fc4-9a90-12d482019586",
            "access_token"      => "APP_USR-2298123634531708-062518-c56d825a88779903fbd8f4894e4e7df9-780464015",
            "test_public_key"   => "TEST-50a371c8-2eb5-4fa8-92bc-b0f66eeff7b7",
            "test_access_token" => "TEST-2298123634531708-062518-86903124ce8fd9ddbd4d617d314ec863-780464015",
            "client_id"         => "2298123634531708",
            "client_secret"     => "NiepEHNwag7eldnkcyOPlxF60pYr4JMc",
        ]
    ]
];
