<?php 
//use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="modal-basic" class="col-xl-7 col-lg-8 col-md-9 col-sm-9">
    <div class="container-fluid">
    	<?php 
    	//category
		$categoryName = $model->category->name;

		//course
        $courseId        = $model->course_id;
        $courseType      = ($model->title == 'basico' ? 'Básico' : 'Avanzado') ;
        $courseIntro     = $model->introduction;
        $courseVideo     = explode("/", $model->videointro);
        $codeVideo       = $courseVideo[3];
        $courseTopics    = $model->topics;
        $courseMaterials = $model->materials;

		//instructor
		$instructorName = $model->instructor->firstname." ".$model->instructor->lastname;
		$instructorImg  = $model->instructor->perfilimage;

        //whatsapp
        $numberWa = "521234567890";
        $urltext = urlencode("Estoy interesado en inscribirme de forma semipresencial al curso de : ".$categoryName." - ".$courseType.", impartido por : ".$instructorName.".");
    	?>
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="modal-title">
                    <?php echo $categoryName; ?>
                </div>
                <div class="float-right modal-type">
                	Curso : <?php echo $courseType; ?>
                </div>
                <div class="clearfix"></div>
                <div class="modal-subtitle">
                	<?php echo $courseIntro; ?>
                </div>
                <div class="modal-video embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/<?php echo $codeVideo; ?>?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="row modal-basic-instructor">
                    <div class="col-5 modal-basic-instructor-picture">
                        <?= Html::img(Url::base().'/backend/web/'.$instructorImg, ['class' => 'img-fluid']); ?>
                    </div>
                    <div class="col-7 modal-basic-instructor-name">
                        <div class="modal-basic-instructor-name-title" class="col-12">Instructor</div>
                        <div class="modal-basic-instructor-name-subtitle" class="col-12"><?php echo $instructorName; ?></div>
                    </div>
                </div>
                <div class="row modal-details">
                    <div class="col-4" style="margin-bottom: 15px; margin-top: -15px;">
                        <div style="background-color: #072879; width: 2px; height: 100%; padding-bottom: 25px; margin-left: 60%;"></div>
                    </div>
                    <div class="col-8 modal-details-item">
                        <div class="row">
                            <div class="col-4 modal-details-item-img">
                              <?= Html::img('@web/images/icon-temario.png', ['class' => 'img-fluid']); ?>  
                            </div>
                            <div class="col-8 modal-details-item-title mt-1">
                                Temario
                            </div>
                            <div class="col-12 modal-details-item-detail">
                            	<?php echo $courseTopics; ?>
                            </div>
                            <div class="col-4 modal-details-item-img">
                              <?= Html::img('@web/images/icon-materiales.png', ['class' => 'img-fluid']); ?>  
                            </div>
                            <div class="col-8 modal-details-item-title mt-1">
                                Materiales
                            </div>
                            <div class="col-12 modal-details-item-detail">
                                <?php echo $courseMaterials; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <!-- <a class="btn btn-modal col-12" >En Línea</a> -->
                                <?= Html::a("En Línea", $url = Url::to(['cursos/inscripcion/','id'=>$courseId]), ['class' => 'btn btn-modal col-12'] )
                                /*Html::a(
                                        "En Línea", 
                                        ['/site/paymentcourse'], 
                                        [
                                            'class' => 'btn btn-modal col-12',
                                            'data'=>[
                                                'method' => 'post',
                                                'params' => ['id' => $courseId]
                                            ]
                                        ]);*/
                                 ?>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <a class="btn btn-modal col-12" target="_blank" href="https://wa.me/<?php echo $numberWa; ?>/?text=<?php echo $urltext ?>">Semipresencial</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>