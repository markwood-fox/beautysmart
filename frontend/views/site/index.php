<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
/*use kartik\icons\Icon;
Icon::map($this);*/
//Icon::map($this, Icon::BSG);
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox-modal'
]);

$this->title = 'Beauty Smart | Transformando la belleza.';
?>
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> -->

<div class="monster-banner">
    <div class="selector-menu">
        <div class="selector-content">
            <div class="column-1 col-6">
                <div class="column-1-content">
                    <div class="normal">
                        <?= Html::img('@web/images/image_educacion.png',['alt'=>'educacion','class'=>'','style'=>'']); ?>
                    </div>
                    <a href="javascript:void(0);" title="Educación" class="ancla" data-ancla="divEducacion">
                        <div class="overlay-normal">
                            <div class="overlay-normal-text">
                                EDU <br> CA <br> CIÓN  <br>
                                <svg xmlns="http://www.w3.org/2000/svg" style="margin-left: 3%;" width="50" height="50" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                                </svg>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div href="" class="column-2 col-6">
                <div class="column-2-content">
                    <div class="small">
                        <?= Html::img('@web/images/image_distribucion.png?v=2',['alt'=>'distribucion','class'=>'','style'=>'']); ?>
                    </div>
                    <a href="javascript:void(0);" title="Distribución" class="ancla" data-ancla="divDistribucion">
                        <div class="overlay-small">
                            <div class="overlay-small-text">
                                DISTRI <br>
                                BUCIÓN <br>
                                <svg xmlns="http://www.w3.org/2000/svg" style="margin-left: 3%;" width="50" height="50" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                                </svg>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="column-2-content">
                    <div class="small">
                        <?= Html::img('@web/images/image_asesoria.png?v=2',['alt'=>'asesoria','class'=>'','style'=>'']); ?>
                    </div>
                    <a href="" title="asesoria">
                        <div class="overlay-small">
                            <div class="overlay-small-text text-asesoria">
                                <div>
                                    ASE <br> SORÍA <br>
                                </div>
                                <div class="overlay-small-subtext text-negocios">
                                    DE NEGOCIOS<br>
                                </div>
                                <div class="arrow-as">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="divEducacion" class="educacion">
    <div class="container">
        <div class="educacion-t">
            En nuestro catálogo encontrarás los entrenamientos más actuales que resuelven las necesidades de especialización y actualización.
        </div>
    </div>
    <div class="slider-content container">
        <div>
            <?php \drmabuse\slick\SlickWidget::widget([
                'container' => '.single-item',
                'settings'  => [
                    'slick' => [
                        'infinite'      =>  true,
                        'slidesToShow'  =>  3,
                        'arrows'=> true,
                        'onBeforeChange'=> new \yii\web\JsExpression('function(){
                        }'),
                        'onAfterChange' => new \yii\web\JsExpression('function(){
                            console.debug(this);
                        }'),
                        'responsive' => [
                            [
                                'breakpoint'=> 1050,
                                  'settings'=> [
                                      'arrows'=> false,
                                      'centerMode'=> false,
                                      'centerPadding'=> 0,
                                      'slidesToShow'=> 2
                                ],
                            ]
                        ],
                    ],
                    //'slickGoTo'         => 3,
                ]
            ]); ?>
            <div class="slider single-item">
                <?php 
                foreach ($categories as $category_item) {
                    $catId    = $category_item->category_id;
                    $catName  = $category_item->name;
                    $catImage = $category_item->mainimage;

                    //related Courses
                    $courses = $category_item->courses;
                ?>
                <div>
                    <div class="slider-item-title"><?php echo $catName; ?></div>
                    <?= Html::img(Url::base()."/backend/web/".$catImage, ['class' => 'img-fluid']) ?>
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-slide-item-top col-12 data-fancybox-modal" data-animation-duration="500" data-src="#modal-instructor" data-touch="false">INSTRUCTOR</button>
                        </div>
                    </div>
                    <div class="row">
                        <?php 
                        //Crouses 
                        if(count($courses) > 0){
                            foreach ($courses as $course_item) {
                                $courseId       = $course_item->course_id;
                                $courseType     = ($course_item->title == 'basico' ? 'Básico' : 'Avanzado');
                                ?>
                                <div class="col-md-6 col-sm-12">
                                    <button class="btn btn-slide-item col-12 data-fancybox-modal" data-type="ajax" data-src="<?php echo Url::to(['site/course','id'=>$courseId]) ?>" data-touch="false"><?php echo $courseType; ?></button>
                                </div>
                                <?php
                            }//end foreach
                        }//end if
                        ?>
                    </div>
                </div>
                <?php
                }//end foreach
                ?>
            </div>
        </div>
    </div>
</section>

<section id="divDistribucion" class="products container">
    <div class="row">
        <div class="products-text-wrap col-md-4">
            <div class="products-text">
                Somos los <strong>distribuidores oficiales</strong> de AVYANA cosmetics en la zona suer de la ciudad de <strong>puebla</strong>, con nosotros encontrarás crecimientos a través de una marca comprometida con el desarrollo de tu negocio y de ti como profesional. 
            </div>
            <div class="products-text-list">
                <ul>
                    <li>Heco en italia</li>
                    <li>Venta exclusiva en salas de belleza</li>
                    <li>Capacitación y asesoría para tu negocio</li>
                    <li>Más calidad al menor precio</li>
                    <li>Tratamiento</li>
                    <li>Color</li>
                    <li>Estilizado</li>
                </ul>
            </div>
        </div>
        <div class="products-items-wrap col-md-8">
            <div class="row">
                <div class="col-md-12 col-lg-6 products-items p-3">
                    <div class="row">
                        <div class="col-4">
                            Imagen
                        </div>
                        <div class="col-8">
                            <div class="product-title">
                                Title Product
                            </div>
                            <div class="product-detail">
                                Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Numquam maxime consectetur temporibus, vero sint deleniti, enim incidunt ullam quam natus ex nemo fugiat! Ipsam neque eveniet excepturi modi obcaecati error beatae, doloribus eius dolorem est, necessitatibus quos molestiae facere soluta ea itaque aliquam libero sed?
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            Comprar
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 products-items p-3">
                    <div class="row">
                        <div class="col-4">
                            Imagen
                        </div>
                        <div class="col-8">
                            <div class="product-title">
                                Title Product
                            </div>
                            <div class="product-detail">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui numquam ullam enim perferendis consectetur, mollitia recusandae eaque nihil quae excepturi dolorum, quas et quasi quisquam earum ducimus quam itaque vero? Soluta, quibusdam maiores quod laboriosam corrupti exercitationem necessitatibus tempora. Molestiae, labore quidem unde ad voluptate?
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            Comprar
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 products-items p-3">
                    <div class="row">
                        <div class="col-4">
                            Imagen
                        </div>
                        <div class="col-8">
                            <div class="product-title">
                                Title Product
                            </div>
                            <div class="product-detail">
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolorem eligendi, error at perferendis in aliquid dignissimos harum saepe, modi exercitationem voluptate fugiat consequatur quod maiores iste adipisci necessitatibus libero. Amet, illo dignissimos consequuntur vero, nostrum corporis dicta, laboriosam ullam voluptas laborum labore voluptatum, id fugiat.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            Comprar
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 products-items p-3">
                    <div class="row">
                        <div class="col-4">
                            Imagen
                        </div>
                        <div class="col-8">
                            <div class="product-title">
                                Title Product
                            </div>
                            <div class="product-detail">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos commodi minus inventore iste nemo excepturi labore sed? Alias dolores rerum incidunt est deserunt magni officiis eligendi! Quo facilis recusandae fuga velit aliquam quia. Soluta illo quis possimus est rem inventore ratione mollitia veritatis sed quisquam!
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            Comprar
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modals PopUp -->
<div style="display: none;" id="modal-instructor" class="col-xl-7 col-lg-8 col-md-9 col-sm-9">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="row modal-instructor-head">
                    <div class="col-4">
                        <?= Html::img('@web/images/foto_instructor.png', ['class' => 'img-fluid']); ?>
                    </div>
                    <div class="col-8">
                        <div class="modal-instructor-title">Instructor</div>
                        <div class="modal-instructor-subtitle">Nombre Apellido</div>
                    </div>
                </div>
                <div class="row modal-instructor-detail">
                    <div class="col-12">
                        <div class="modal-instructor-detail-title">
                            Trayectoria
                        </div>
                        <div class="modal-instructor-detail-text">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque, id, impedit. Suscipit fuga non, nisi esse, dignissimos reiciendis maxime facere. Alias, laborum suscipit at error, sit quam autem ducimus hic praesentium maxime, eius reiciendis obcaecati modi incidunt laudantium natus perspiciatis!
                            <br>
                            Lorem ipsum, dolor sit amet, consectetur adipisicing elit. Recusandae sapiente, consequatur quas. Dolores quasi animi eum sed, veniam, obcaecati autem.
                        </div>
                    </div>
                </div>
                <div class="row modal-instructor-share">
                    <div class="col-12">
                        <div class="modal-instructor-share-title">
                            Redes Sociales
                        </div>
                    </div>
                    <div class="row modal-instructor-share-icons">
                        <div class="col-4">
                            <?php echo Html::a(Html::img('@web/images/ig-icon.svg',['class'=>'img-fluid','style'=>'width: 35px; height: auto;']), $url = Yii::$app->homeUrl,['title'=>'Instagram']) ?>
                        </div>
                        <div class="col-4">
                            <?php echo Html::a(Html::img('@web/images/wa-icon.svg',['class'=>'img-fluid','style'=>'width: 35px; height: auto;']),$url = Yii::$app->homeUrl,['title'=>'whatsapp']) ?>
                        </div>
                        <div class="col-4">
                            <?php echo Html::a(Html::img('@web/images/fb-icon.svg',['class'=>'img-fluid','style'=>'width: 35px; height: auto;']),$url = Yii::$app->homeUrl,['title'=>'facebook']) ?>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="col-md-6 col-12">
                <div class="row">
                    <div class="col-6">
                        <?= Html::img('@web/images/foto_popup1.png', ['class' => 'img-fluid modal-instructor-gallery']); ?>
                        <?= Html::img('@web/images/foto_popup4.png', ['class' => 'img-fluid modal-instructor-gallery']); ?>
                    </div>
                    <div class="col-6">
                        <?= Html::img('@web/images/foto_popup3.png', ['class' => 'img-fluid modal-instructor-gallery']); ?>
                        <?= Html::img('@web/images/foto_popup2.png', ['class' => 'img-fluid modal-instructor-gallery']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modals PopUp -->
<?php
$script = <<< JS
$(function(e){
    $(".ancla").click(function(e){
        var divAncla = "#"+$(this).data("ancla");
        $("html,body").animate({scrollTop: ($(divAncla).offset().top - 100) }, 1000);
    });
});
JS;
$this->registerJs($script);
?>
<?php $this->registerJsFile('https://www.mercadopago.com/v2/security.js',['depends' => [\yii\web\JqueryAsset::className()],'view'=>'home']); ?>