<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use kartik\icons\Icon;
Icon::map($this);

echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox-modal',
    'config' => [
    	'modal' => true,
    	'clickSlide'        => false,
    	'clickOutside'      => false,
    ]
]);

$categoryname = $model->category->name;
$coursename   = $model->title == 'basico' ? 'Básico' : 'Avanzado';
$this->title  = $model->category->name." Curso ".$coursename.' | Beauty Smart';
//$this->params['breadcrumbs'][] = $this->title;

//mercadopago
$public_key = Yii::$app->params["mercadopago"]["dev"]["seller"]["test_public_key"];
?>
<style type="text/css">
	.error{
		display: block;
		margin: 0;
	}

	.error input{
		border: 1px solid red !important;
	}
</style>

<div class="container">
	<?php $form = ActiveForm::begin([
		'action'  => ['site/createpayment'],
		'validateOnSubmit' => false,
		'options' => [
				'enctype'      => 'multipart/form-data',
				'id'           => 'form-checkout',
				'class'        => 'mb-5',
				//'onsubmit' => 'showPaymentWait();',
			],

		]); ?>
		<div class="row">
			<div class="col-sm-12 col-lg-8">
				<div class="order-detail">
					<h2>Inscripción al curso</h2>
					<div class="card border-secondary bg-light">
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-xl-3 text-center">
									<?= Html::img(Url::base()."/backend/web/".$model->category->mainimage, ['class' => 'img-fluid']) ?>
								</div>
								<div class="col-12 col-sm-6 col-xl-9">
									<h3>
										<?php echo $categoryname; ?>
									</h3>
									<h4 class="card-title">
										<?php echo "Curso: ".$coursename ?>
									</h4>
									<h4 class="card-title">
										<?php echo "Instructor: ".$model->instructor->firstname." ".$model->instructor->lastname; ?>
									</h4>
									<p class="card-text">
										<?php echo $model->introduction; ?>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="order-detail mt-4">
					<h2>
						Identificación
					</h2>
					<div class="card border-secondary bg-light">
						<div class="card-body">
							<div class="row">
								<div class="col-12">
									<h5>
										Solicitamos únicamente la información esencial para la finalización de la compra.
									</h5>
									<div class="row pt-2">
										<div class="form-group col-12 col-lg-6">
											<?php echo $form->field($modelPayment,'namepayment',['options'=>['class'=>'']])->textInput(['placeholder'=>'Nombre(s)','required'=>'required']); ?>
											<label id="paymentcourse-namepayment-error" class="error invalid-feedback" for="paymentcourse-namepayment" style="display: none;"></label>
										</div>
										<div class="form-group col-12 col-lg-6">
											<?php echo $form->field($modelPayment,'lastnamepayment',['options'=>['class'=>'']])->textInput(['placeholder'=>'Apellido(s)','required'=>'required']); ?>
											<label id="paymentcourse-lastnamepayment-error" class="error invalid-feedback" for="paymentcourse-lastnamepayment" style="display: none;"></label>
										</div>
										<div class="form-group col-12 col-lg-6">
											<?= $form->field($modelPayment, 'email',['options'=>['class'=>''],'inputOptions' => ['type'=>'email']])->textInput(['placeholder'=>'email@example.com','required'=>'required']) ?>
											<label id="paymentcourse-email-error" class="error invalid-feedback" for="paymentcourse-email" style="display: none;"></label>
										</div>
										<div class="form-group col-12 col-lg-6">
											<?= $form->field($modelPayment, 'phonepayment',['options'=>['class'=>''],'inputOptions' => ['type'=>'number']])->textInput(['placeholder'=>'Teléfono','required'=>'required','minlength'=>10,'digits'=>'true']) ?>
											<label id="paymentcourse-phonepayment-error" class="error invalid-feedback" for="paymentcourse-phonepayment" style="display: none;"></label>
											<!-- <label for="phonepayment">Teléfono</label>
											<input type="number" class="form-control" id="phonepayment" placeholder="Teléfono"> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-lg-4">
				<h2>Información de Pago</h2>
				<div class="card border-secondary bg-light mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-12 text-center">
								<?= Html::img("@web/images/formas-de-pago-mercadopago.png", ['class' => 'img-fluid']) ?>
							</div>
						</div>
						<div id="alert-mp" class="row mt-4" style="display: none;"></div>
						<div class="row mt-4">
							<div class="col-12 form-group">
								<?php echo $form->field($modelPayment,'cardNumber',[
									'options'=>['class'=>''],
									'inputOptions' => [
										'type'=>'number',
										'name'=>false,
										'id'=>'cardNumber']
									])->textInput(
										[
											'required'=>'required',
											'minlength'=>15,
											'digits'=>'true',
											'data-checkout'=>'cardNumber',
											'onselectstart'=>'return false',
											'onpaste'=>'return false',
											'oncopy'=>'return false',
											'oncut'=>'return false',
											'ondrag'=>'return false',
											'ondrop'=>'return false',
											'autocomplete'=>'off'
										]
									); ?>
								<label id="cardNumber-error" class="error invalid-feedback" for="cardNumber" style="display: none;"></label>
							</div>
							<div class="col-12 form-group">
								<?php 
									echo $form->field($modelPayment, 'issuer',[
										'options'=>['class'=>''],
										'inputOptions'=>[
											'name'=>'issuer',
											'id'=>'issuer']
										])->dropDownList(
											[], 
											[
												'required'=>'required',
												'data-checkout'=>'issuer',
											]
										)->label('Banco Emisor');
								?>
								<label id="issuer-error" class="error invalid-feedback" for="issuer" style="display: none;"></label>
							</div>

							<div class="col-12 form-group">
								<?php 
									echo $form->field($modelPayment,'cardholderName',[
										'options'=>['class'=>''],
										'inputOptions'=>[
											'name'=>false,
											'id'=>'cardholderName']
										])->textInput(
											[
												'data-checkout'=>'cardholderName',
												'required'=>'required',
											]
										);?>

								<label id="cardholderName-error" class="error invalid-feedback" for="cardholderName" style="display: none;"></label>
							</div>
							<div class="col-12 pt-3">
								<?php echo Html::label('Fecha de vencimiento', '', ['class' => 'label']) ?>
							</div>
							<div class="col-6 form-group">
								<?php
									$mm = ["01"=>"01","02"=>"02","03"=>"03","04"=>"04","05"=>"05","06"=>"06","07"=>"07","08"=>"08","09"=>"09","10"=>"10","11"=>"11","12"=>"12"];
									echo $form->field($modelPayment, 'cardExpirationMonth',[
										'options'=>['class'=>''],
										'inputOptions'=>[
											'name'=>false,
											'id'=>'cardExpirationMonth'
										]])->dropDownList(
											$mm, 
											[
												'prompt' => 'MM',
												'required'=>'required',
												'data-checkout'=>'cardExpirationMonth',
												'onselectstart'=>'return false',
												'onpaste'=>'return false',
												'oncopy'=>'return false',
												'oncut'=>'return false',
												'ondrag'=>'return false',
												'ondrop'=>'return false',
												'autocomplete'=>'off'
											]
										)->label(false);
								?>
								<label id="cardExpirationMonth-error" class="error invalid-feedback" for="cardExpirationMonth" style="display: none;"></label>
							</div>
							<div class="col-6 form-group">
								<?php 
									$yrs     = date('Y');
									$arr_yrs = [];
									for ($i=$yrs; $i <= ($yrs + 20) ; $i++) { 
										$arr_yrs[$i] = $i;
									}//end for
									echo $form->field($modelPayment, 'cardExpirationYear',[
										'options'=>['class'=>''],
										'inputOptions'=>[
											'name'=>false,'id'=>'cardExpirationYear'
										]])->dropDownList(
											$arr_yrs,
											[
												'prompt' => 'AAAA',
												'required'=>'required',
												'data-checkout'=>'cardExpirationYear',
												'onselectstart'=>'return false',
												'onpaste'=>'return false',
												'oncopy'=>'return false',
												'oncut'=>'return false',
												'ondrag'=>'return false',
												'ondrop'=>'return false',
												'autocomplete'=>'off'
											]
										)->label(false);
								?>
								<label id="cardExpirationYear-error" class="error invalid-feedback" for="cardExpirationYear" style="display: none;"></label>
							</div>
							<div class="col-12 pt-3">
								<?php echo  Html::label('Código de Seguridad', 'securityCode', ['class' => 'label']) ?>
							</div>
							<div class="col-8">
								<?php echo $form->field($modelPayment,'securityCode',[
									'options'=>['class'=>''],
									'inputOptions' => [
										'type'=>'password',
										'name'=>false,
										'id'=>'securityCode']
									])->textInput([
										'required'=>'required',
										'minlength' => 3,
										'maxlength' => 4,
										'data-checkout'=>'securityCode',
										'onselectstart'=>'return false',
										'onpaste'=>'return false',
										'oncopy'=>'return false',
										'oncut'=>'return false',
										'ondrag'=>'return false',
										'ondrop'=>'return false',
										'autocomplete'=>'off','placeholder'=>'****'
									])->label(false); ?>
								<label id="securityCode-error" class="error invalid-feedback" for="securityCode" style="display: none;"></label>
							</div>
							<div class="col-4">
								<?php echo  Html::img("@web/images/cvv.png", ['class' => 'img-fluid']) ?>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-12 text-center">
								<p class="h1">Total <?php echo " $ ".number_format($model->price, 2, '.', ',')." MXN" ?></p>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-12 text-justify">
								<?php 
									echo $form->field($modelPayment,'acceptconditions')->checkBox([
										'required'=>'required',
										'data-msg'=>'Debe leer y aceptar los términos y condiciones.',
										'checked' => false
									])->label('He leído Política de privacidad y Términos y condiciones'); 
								?>
								<label id="PaymentCourse[acceptconditions]-error" class="error invalid-feedback" for="PaymentCourse[acceptconditions]" style="display: none;"></label>
							</div>
						</div>
						<div class="col-12" id="divinstallments" style="display: none;">
							<?php 
								echo $form->field($modelPayment, 'installments',['inputOptions'=>['name'=>'installments','id'=>'installments']])->dropDownList([], ['data-checkout'=>'installments',])->label('Cuotas');
							?>
						</div>
						<div class="row mt-4">
							<?php echo $form->field($modelPayment,'transactionAmount',['inputOptions'=>['id'=>'transactionAmount']])->hiddenInput(['value'=>$model->price])->label(false);?>
							<?php echo $form->field($modelPayment,'paymentMethodId',['inputOptions'=>['id'=>'paymentMethodId']])->hiddenInput()->label(false);?>
							<?php echo $form->field($modelPayment,'description',['inputOptions'=>['id'=>'description','value'=>'Curso: '.$categoryname." (".$coursename.")"]])->hiddenInput()->label(false);?>
							<?php echo $form->field($modelPayment,'courseId',['inputOptions'=>['id'=>'courseId','value'=>$model->course_id]])->hiddenInput()->label(false);?>
							<?= Html::button(Html::tag('i','',['class'=>'fas fa-lock'])." Realizar Pago Seguro", ['id'=>'btnPayment','class' => 'btn btn-success col-12 btn-payment']); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php ActiveForm::end(); ?>
</div>

<!-- Modals PopUp -->
<button class="btn btn-slide-item-top col-12 data-fancybox-modal" data-animation-duration="500" data-src="#modal-paymentwait" data-touch="false" style="display: none;">INSTRUCTOR</button>
<div style="display: none;" id="modal-paymentwait" class="col-md-4 col-sm-12 alert alert-info">
    <div class="container-fluid">
        <div class="row">
        	<div class="col-3">
        		<?= Html::img("@web/images/loading.gif", ['class' => 'img-fluid']) ?>
        	</div>
        	<div class="col-9">
        		<h3 class="modal-title">Muchas gracias.</h3>
        		<br>
        		<h5 class="modal-subtitle">
            		Su información de pago se está procesando, porfavor no cierre ni actualice la ventana.
        		</h5>
        	</div>
        </div>
    </div>
</div>
<!-- End Modals PopUp -->
<?php
$script = <<< JS
	window.Mercadopago.setPublishableKey('$public_key');

	document.getElementById('cardNumber').addEventListener('change', guessPaymentMethod);
	function guessPaymentMethod(event) {
	   let cardnumber = document.getElementById("cardNumber").value;
	   if (cardnumber.length >= 6) {
	       let bin = cardnumber.substring(0,6);
	       window.Mercadopago.getPaymentMethod({
	           "bin": bin
	       }, setPaymentMethod);
	   }
	};

	function setPaymentMethod(status, response) {
	   if (status == 200) {
	   		let paymentMethod = response[0];
	   		document.getElementById('paymentMethodId').value = paymentMethod.id;
	   		getIssuers(paymentMethod.id);

	   		//clean alert MP
			$("#alert-mp").empty();
			$("#alert-mp").hide(function(e){});
	   } else {
	   		$("#alert-mp").html('<div class="alert alert-danger col-12 text-center" style="text-transform: uppercase;">'+response.message+'</div>');
	   		$("#alert-mp").show();
	   		$("#paymentMethodId").val("");
	   		$("#issuer").empty();
	   		$("#installments").empty();
	   		console.log('payment method info error :' + JSON.stringify(response, null, 4));
	   }
	}

	function getIssuers(paymentMethodId) {
	   window.Mercadopago.getIssuers(
	       paymentMethodId,
	       setIssuers
	   );
	}

	function setIssuers(status, response) {
	   if (status == 200) {
	   		let issuerSelect = document.getElementById('issuer');
	   		response.forEach( issuer => {
	   			let opt = document.createElement('option');
	   			opt.text = issuer.name;
	   			opt.value = issuer.id;
	   			issuerSelect.appendChild(opt);
	   		});

	   		getInstallments(document.getElementById('paymentMethodId').value,document.getElementById('transactionAmount').value,issuerSelect.value);
	       	
	       	//clean alert MP
			$("#alert-mp").empty();
			$("#alert-mp").hide(function(e){});
	   } else {
	   		$("#alert-mp").html('<div class="alert alert-danger col-12 text-center" style="text-transform: uppercase;">'+response.message+'</div>');
	   		$("#alert-mp").show();
	       	console.log('issuers method info error:' +  JSON.stringify(response, null, 4));
	   }
	}

	function getInstallments(paymentMethodId, transactionAmount, issuerId){
		window.Mercadopago.getInstallments({
			"payment_method_id": paymentMethodId,
			"amount": parseFloat(transactionAmount),
			"issuer_id": parseInt(issuerId)
		}, setInstallments);
	}

	function setInstallments(status, response){
	   if (status == 200) {
	   		document.getElementById('installments').options.length = 0;
	   		response[0].payer_costs.forEach( payerCost => {
				let opt   = document.createElement('option');
				opt.text  = payerCost.recommended_message;
				opt.value = payerCost.installments;
	   			document.getElementById('installments').appendChild(opt);
	   		});

	       	//clean alert MP
			$("#alert-mp").empty();
			$("#alert-mp").hide(function(e){});
	   } else {
	   		$("#alert-mp").html('<div class="alert alert-danger col-12 text-center" style="text-transform: uppercase;">'+response.message+'</div>');
	   		$("#alert-mp").show();
	       	console.log('installments method info error:' + JSON.stringify(response, null, 4));
	   }
	}

	function setCardTokenAndPay(status, response) {
	   	if (status == 200 || status == 201) {
	   		//show modal paymentwait
	   		$(".data-fancybox-modal").trigger("click");
	       	let form = document.getElementById('form-checkout');
	       	let card = document.createElement('input');
	       	card.setAttribute('name', 'token');
	       	card.setAttribute('type', 'hidden');
	       	card.setAttribute('value', response.id);
	       	form.appendChild(card);
	       	//doSubmit=true;
	       	form.submit();
	       	//return false;
	   	} else {
			let error    = response.error;
			let cause    = response.cause;
			let msgerror = "";
			let flag = false;
			if(error == 'bad_request'){
				cause.forEach(function(elemento, indice, array){
					if(elemento.code == "325" || elemento.code == "326"){
						if(flag == false){
							msgerror += "<div class='alert alert-danger col-12 text-center' style='text-transform: uppercase;'>Verifique la fecha de vencimiento</div>";
						}
						flag = true;
					}

					if(elemento.code == "E301"){
						msgerror += "<div class='alert alert-danger col-12 text-center' style='text-transform: uppercase;'>El número de tarjeta es incorrecto</div>";	
					}

					if(elemento.code == "E302"){
						msgerror += "<div class='alert alert-danger col-12 text-center' style='text-transform: uppercase;'>El código de seguridad es incorrecto</div>";	
					}


					//console.log(elemento);
				});

				if(msgerror != ""){
					$("#alert-mp").html(msgerror);
			   		$("#alert-mp").show();

			   		$('html, body').animate({
			   			scrollTop: $("#alert-mp").offset().top - 106
			   			}, 1000);
			   	}
			}//end if

			//return false;
	   		//console.log(error + cause);
	       	//alert("Verify filled data! " + JSON.stringify(response, null, 4));
	   	}
	}//end function


	//Send Payment Data
	$("#btnPayment").on("click",function(e){
		var validForm           = $("#form-checkout").valid({lang: 'es'});
		var cardholderName      = $('#cardholderName').valid();
		var cardExpirationMonth = $('#cardExpirationMonth').valid();
		var cardExpirationYear  = $('#cardExpirationYear').valid();
		var securityCode        = $('#securityCode').valid();

		if(validForm == true && cardholderName == true && cardExpirationMonth == true && cardExpirationYear == true && securityCode == true){
			//Genera el token de mercado pago
			let form = document.getElementById('form-checkout');
			window.Mercadopago.createToken(form, setCardTokenAndPay);
		}//end if

		return false;
	});

JS;
$this->registerJs($script);
?>
<?php $this->registerJsFile('https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile('@web/js/messages_es.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
