<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use kartik\icons\Icon;
Icon::map($this);

$categoryname = $model->category->name;
$coursename   = $model->title == 'basico' ? 'Básico' : 'Avanzado';
$this->title  = $model->category->name." Curso ".$coursename.' | Beauty Smart';
//$this->params['breadcrumbs'][] = $this->title;

/*echo "<pre>";
var_dump($preference_mp);
echo "</pre>";
die();*/
?>
<style type="text/css">
	.error{
		display: block;
		margin: 0;
	}

	.error input{
		border: 1px solid red !important;
	}
</style>

<div class="container">
	<div class="row">
		<div class="col-12 mb-5">
			<div class="order-detail">
				<h2>Inscripción al curso</h2>
				<div class="card border-secondary bg-light">
					<div class="card-body">
						<div class="row">
							<div class="col-12 col-sm-6 col-xl-3 text-center">
								<?= Html::img(Url::base()."/backend/web/".$model->category->mainimage, ['class' => 'img-fluid']) ?>
							</div>
							<div class="col-12 col-sm-6 col-xl-9">
								<h1>
									<?php echo $categoryname; ?>
								</h1>
								<h2 class="card-title">
									<?php echo "Curso: ".$coursename ?>
								</h2>
								<h3 class="card-title">
									<?php echo "Instructor: ".$model->instructor->firstname." ".$model->instructor->lastname; ?>
								</h3>
								<p class="card-body">
									<?php echo $model->introduction; ?>
								</p>

								<div class="card-body">
									<div class="row">
										<div class="col-12">
											<?php $form = ActiveForm::begin([
												'action'  => ['cursos/creaprefencia'],
												'validateOnSubmit' => false,
												'options' => [
														'enctype'      => 'multipart/form-data',
														'id'           => 'form-checkout',
														'class'        => 'mb-5',
														//'onsubmit' => 'showPaymentWait();',
													],

												]); ?>
											<div class="row">
												<div class="form-group col-lg-6 col-12">
													<?php echo $form->field($modelProfile,'firstname',['options'=>['class'=>'']])->textInput(['placeholder'=>'Nombre(s)','required'=>'required']); ?>
													<label id="profile-firstname-error" class="error invalid-feedback" for="profile-firstname" style="display: none;"></label>
												</div>
												<div class="form-group col-lg-6 col-12">
													<?php echo $form->field($modelProfile,'lastname',['options'=>['class'=>'']])->textInput(['placeholder'=>'Apellidos','required'=>'required']); ?>
													<label id="profile-lastname-error" class="error invalid-feedback" for="profile-lastname" style="display: none;"></label>
												</div>
												<div class="form-group col-lg-6 col-12">
													<?php echo $form->field($modelProfile,'email',['options'=>['class'=>'']])->textInput(['placeholder'=>'correo@dominio.com','required'=>'required']); ?>
													<label id="profile-email-error" class="error invalid-feedback" for="profile-email" style="display: none;"></label>
												</div>
												<div class="form-group col-lg-6 col-12">
													<?php echo $form->field($modelProfile, 'phone',['options'=>['class'=>''],'inputOptions' => ['type'=>'number']])->textInput(['placeholder'=>'0000000000','required'=>true,'maxlength'=>10,'minlength'=>10,'digits'=>'true']) ?>
													<label id="profile-phone-error" class="error invalid-feedback" for="profile-phone" style="display: none;"></label>
												</div>
												<div class="col-12 text-center">
													<p class="h1">Total: <?php echo " $ ".number_format($model->price, 2, '.', ',')." MXN" ?></p>
												</div>
												<div class="col-12 text-center">
													<?= Html::img("@web/images/formas-de-pago-mercadopago.png", ['class' => 'img-fluid']) ?>
												</div>
												<div class="col-12 text-center">
													<?php echo  Html::hiddenInput('course_id', $value = $model->course_id, ['option' => 'value']); ?>
													<?php echo  Html::button(Html::tag('i','',['class'=>'fas fa-lock'])." Realizar Pago Seguro", ['id'=>'btnPayment','class' => 'btn btn-success col-lg-7 col-12 btn-payment']); ?>
												</div>
											</div>
											<?php ActiveForm::end(); ?>
										</div>
										<div class="col-lg-8 col-12 text-center">
											
											<?php 
												/*echo "<a href='".$preference_mp->init_point."'>
													<button type='button' class='btn btn-success col-8 btn-payment' formmethod='post'>
														<i class='fas fa-lock'></i> Realizar Pago Seguro
													</button>
												</a>";*/
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$script = <<< JS
	//Send Payment Data
	$("#btnPayment").on("click",function(e){
		var validForm           = $("#form-checkout").valid({lang: 'es'});
		if(validForm == true){
			$.ajax({
				type: "POST",
				url: $("#form-checkout").attr('action'),
				data: $("#form-checkout").serialize(),
				beforeSend: function(data){
					$("#btnPayment").prop('disabled', true);
					$("#btnPayment").html('Creando referencias de pago....');
				},
				success: function(data){
					//$("#btnPayment").prop('disabled', false);
					$("#btnPayment").html('Redireccionando a Mercado Pago ...');
					const response =  JSON.parse(data);

					
					if(response.status == true){
						location.href = response.init_point;
						return false;
					}else if(response.status == false){
						if(response.code == 400){
							$("#form-checkout").empty();
							$("#form-checkout").html(response.div);
						}else if(response.code == 402){
							$("#form-checkout").empty();
							$("#form-checkout").html(response.div);
						}else{
							$("#btnPayment").hide();
							$("#btnPayment").before("<div class='col-12'><div class='alert alert-danger'>No se pudo lograr conectividad con Mercado Pago, por favor intente más tarde.</div></div>");
						}//end if
					}//end if
					return false;
				},
			});
		}//end if
	});
JS;
$this->registerJs($script);
?>
<?php $this->registerJsFile('https://www.mercadopago.com/v2/security.js',['depends' => [\yii\web\JqueryAsset::className()],'view'=>'item']); ?>
<?php $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile('@web/js/messages_es.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
