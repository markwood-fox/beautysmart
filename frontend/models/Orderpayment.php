<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%orderpayment}}".
 *
 * @property int $orderpayment_id
 * @property int $order_id
 * @property string|null $code
 * @property string|null $orderTransactionId
 * @property string|null $state
 * @property string|null $trazabilityCode
 * @property string|null $authorizationCode
 * @property string|null $responseCode
 * @property string|null $operationDate
 * @property string|null $updateDate
 * @property string|null $approvedDate
 * @property string|null $pendingreason
 * @property string|null $expirationdate
 * @property string|null $reference
 * @property string|null $barcode
 * @property string|null $urlpayment
 * @property float|null $totalpayment
 *
 * @property Order $order
 */
class Orderpayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%orderpayment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id'], 'integer'],
            [['totalpayment'], 'number'],
            [['code', 'reference'], 'string', 'max' => 45],
            [['orderTransactionId', 'trazabilityCode', 'authorizationCode', 'responseCode', 'operationDate', 'updateDate', 'approvedDate'], 'string', 'max' => 150],
            [['state', 'expirationdate'], 'string', 'max' => 60],
            [['pendingreason'], 'string', 'max' => 200],
            [['barcode'], 'string', 'max' => 50],
            [['urlpayment'], 'string', 'max' => 185],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'orderpayment_id' => 'Orderpayment ID',
            'order_id' => 'Order ID',
            'code' => 'Code',
            'orderTransactionId' => 'Order Transaction ID',
            'state' => 'State',
            'trazabilityCode' => 'Trazability Code',
            'authorizationCode' => 'Authorization Code',
            'responseCode' => 'Response Code',
            'operationDate' => 'Operation Date',
            'updateDate' => 'Update Date',
            'approvedDate' => 'Approved Date',
            'pendingreason' => 'Pendingreason',
            'expirationdate' => 'Expirationdate',
            'reference' => 'Reference',
            'barcode' => 'Barcode',
            'urlpayment' => 'Urlpayment',
            'totalpayment' => 'Totalpayment',
        ];
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
