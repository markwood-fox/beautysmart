<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Contact;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $captcha;
    public $reCaptcha;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            [['body'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 150],
            [['subject'], 'string', 'max' => 250],
            // email has to be a valid email address
            ['email', 'email'],
            //Recaptcha Google V3
            [['reCaptcha'], \kekaadrenalin\recaptcha3\ReCaptchaValidator::className(), 'acceptance_score' => 0.8]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name'       => 'Nombre',
            'email'      => 'Correo eléctronico',
            'subject'    => 'Asunto',
            'body'       => 'Comentarios',
        ];
    }

    public function saveContact(){
        if (!$this->validate()) {
            return null;
        }

        $contact             = new Contact();
        $contact->name       = $this->name;
        $contact->email      = $this->email;
        $contact->subject    = $this->subject;
        $contact->body       = $this->body;
        $contact->created_at = date('Y-m-d H:i:s');
        return $contact->save();
    }//end function

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
            ->setReplyTo([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
}
