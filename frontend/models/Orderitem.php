<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%orderitem}}".
 *
 * @property int $order_id
 * @property int|null $product_id
 * @property string|null $typeproduct
 * @property string|null $sku
 * @property string|null $name
 * @property float|null $price
 * @property int|null $quantity
 *
 * @property Order $order
 */
class Orderitem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%orderitem}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'product_id', 'quantity'], 'integer'],
            [['price'], 'number'],
            [['typeproduct'], 'string', 'max' => 15],
            [['sku', 'name'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'typeproduct' => 'Typeproduct',
            'sku' => 'Sku',
            'name' => 'Name',
            'price' => 'Price',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
