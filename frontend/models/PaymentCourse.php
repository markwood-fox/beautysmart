<?php 
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * 
 */
class PaymentCourse extends Model
{
	//identity
	public $namepayment;
	public $lastnamepayment;
	public $email;
	public $phonepayment;

	//course
	public $courseId;

	//payment
	public $cardNumber;
	public $cardholderName;
	public $cardExpirationMonth;
	public $cardExpirationYear;
	public $securityCode;
	public $acceptconditions;
	public $transactionAmount;
	public $paymentMethodId;
	public $description;
	public $issuer;
	public $installments;
	
	public function rules(){
		return [
			/*[['namepayment','lastnamepayment','emailpayment','phonepayment','cardNumber','cardholderName','cardExpirationMonth','cardExpirationYear','securityCode','issuer','installments'],'required'],
			[['acceptconditions'],'required', 'requiredValue' => 1,'message' => 'Debe leer y aceptar las políticas de privacidad y términos y condiciones'],
			[['emailpayment'],'email'],
			[['phonepayment'],'string','min'=>10],
			[['phonepayment'],'integer'],
			[['cardNumber'],'string','min'=>15],
			[['cardNumber'],'integer'],
			[['securityCode'],'string','min'=>3,'max'=>4],
			[['securityCode'],'integer'],*/

		];
	}

	public function attributeLabels()
    {
        return [
			'namepayment'         => 'Nombre(s)',
			'lastnamepayment'     => 'Apellido(s)',
			'emailpayment'        => 'Email',
			'phonepayment'        => 'Teléfono',
			'cardNumber'          => 'Número de Tarjeta',
			'cardholderName'      => 'Titular de la tarjeta',
			'cardExpirationMonth' => 'Mes de vencimiento',
			'cardExpirationYear'  => 'Año de vencimiento',
			'securityCode'        => 'Código de Seguridad',
			'acceptconditions'    => 'Política de privacidad - Términos y Condiciones',
			'issuer'              => 'Banco Emisor',
			'installments'        => 'Cuotas',

        ];
    }
}
