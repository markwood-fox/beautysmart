<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

//common
use common\models\User;

//frontend
use frontend\models\SignupForm;
use frontend\models\PaymentCourse;
use frontend\models\Profile;
use frontend\models\Order;
use frontend\models\Orderitem;
use frontend\models\Orderpayment;
use frontend\models\VerifyEmailForm;

//Backend
use backend\models\Category;
use backend\models\Course;

//Mercado Pago
use MercadoPago\SDK;
use MercadoPago\Payment;
use MercadoPago\Payer;
use MercadoPago\Preference;
use MercadoPago\Item;

/**
 * Course controller
 */
class CursosController extends Controller
{
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Crate external referene code for payment
     */
    public function alfaCode($length){
        $alfa    = '';
        $key     = '';
        $pattern = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $max     = strlen($pattern)-1;

        for($i = 0;$i < $length;$i++){
            $alfa .= $pattern[mt_rand(0,$max)];
        }

        $date    = date('ymdhis');
        $key     = $alfa.$date;
        return $key;
    }//end function

    //Form to Pay Course
    public function actionInscripcion(){
        $idCourse = Yii::$app->request->get();
        if(isset($idCourse["id"])){
            $model        = Course::findOne(['course_id'=>$idCourse]);

            if(!is_null($model)){
	            //$modelPayment = new PaymentCourse();
                $modelProfile = new Profile();
	            return $this->render('paymentcourse',[
                        'model'         => $model,
                        'modelProfile'  => $modelProfile,
                        //'preference_mp' => $preference
	                ]);
            }else{
            	return $this->redirect(['/']);	
            }//end if
        }else{
            return $this->redirect(['/']);
        }
    }//end function


    /**
     * Search Orders related profile email
     */
    private function searchOrdersByProfile($idCourse = null, $email = null){
        //$searchProfile = Profile::findOne(["email"=>$email]);
        $results = (new \yii\db\Query())
            ->select(['profile.profile_id','profile.email','profile.firstname','profile.lastname','order.status','orderitem.product_id','orderPayment.*'])
            ->from('order')
            ->join('INNER JOIN','profile','order.profile_id = profile.profile_id')
            ->join('INNER JOIN','orderitem','orderitem.order_id = order.order_id')
            ->join('INNER JOIN','orderpayment','orderpayment.order_id = order.order_id')
            ->where(['profile.email' => $email,'orderitem.product_id'=>$idCourse])
            ->orderBy(['order.status'=>SORT_ASC])
            ->all();

        $response = [];
        if(count($results) > 0){
            foreach ($results as $order) {
                $status = $order["status"];
                if($status == "approved"){
                    //Error Inscription create
                    $response = [
                        "code"=>400,
                        "profile" => [
                            "email" => $order["email"],
                            "name"  => $order["firstname"]." ".$order["lastname"]
                        ]
                    ]; //Bad Request
                    break;
                }elseif($status == "pending"){
                    //Error Inscription Pending
                    $response = [
                        "code"    => 402,
                        "profile" => [
                            "email" => $order["email"],
                            "name"  => $order["firstname"]." ".$order["lastname"]
                        ],
                        "urlpayment" => $order["urlpayment"],
                    ]; //Payment Required
                    break;
                }elseif($status == "rejected"){
                    //Success Payment Continue
                    $response = ["code"=>200]; //Success
                    break;
                }//end if
            }//end foreach
        }else{
            $response = ["code"=>200]; //Success
        }//end if

        return $response;
    }//end function

    /**
     * Create Preference Mercado Pago
     */
    public function actionCreaprefencia(){
        $profile  = Yii::$app->request->post()["Profile"];
        $idCourse = Yii::$app->request->post()["course_id"];

        /**
         * Search Profile in orders for not payment duplicate 
         */
        $searchProfile = $this->searchOrdersByProfile($idCourse,$profile["email"]);
        if($searchProfile["code"] == 200){

            //Course Info
            $model     = Course::findOne(['course_id'=>$idCourse]);

            $categoryname = $model->category->name;
            $coursename   = $model->title == 'basico' ? 'Básico' : 'Avanzado';

            //Mercado Pago Access Token Seller
            $access_token = Yii::$app->params["mercadopago"]["dev"]["seller"]["access_token"];

            //Mercado Pago 
            //Init SDK
            $mp     = new SDK();
            $mp->initialize();
            $config = $mp->config();
            $config->set('ACCESS_TOKEN', $access_token);

            //Create payment preference
            $preference =  new Preference();

            //Create Item information
            $item              = new Item();
            $item->id          = $model->code;
            $item->category_id = "education";
            $item->title       = $categoryname." Curso: ".$coursename;
            $item->description = "Inscripción al curso : ".$categoryname." ".$coursename;
            $item->quantity    = 1;
            $item->unit_price  = (float) $model->price;
            $item->picture_url = Url::home('http')."backend/web/".$model->category->mainimage;
            $item->currency_id = "MXN";
            //Preference Items
            $preference->items = [$item];

            //Mercado Pago Payer Information
            $payer             = new Payer();
            $payer->email      = $profile["email"];
            $payer->name       = $profile["firstname"];
            $payer->surname    = $profile["lastname"];
            $payer->phone    = [
                "area_code" => "52",
                "number"    => $profile["phone"]
            ];
            //Preference Payer
            $preference->payer = $payer;

            //Url return response
            $preference->back_urls = [
                "success" => Url::home('http')."cursos/pago?id=success",
                "failure" => Url::home('http')."cursos/pago?id=failure",
                "pending" => Url::home('http')."cursos/pago?id=pending"
            ];
            $preference->auto_return = "approved";

            $preference->payment_methods = [
                "excluded_payment_methods" => [
                    // [
                    //     "id" => "amex"
                    // ]
                ],
                "excluded_payment_types" => [
                    [
                        "id" => "atm"
                    ]
                ],
                "installments" => 1
            ];

            $preference->external_reference   = $this->alfaCode(4);
            $preference->statement_descriptor = 'Beauty Smart';
            $preference->binary_mode          = true;

            //Webhook
            //$preference->notification_url   = Url::home('http')."backend/web/webhooks.php";

            //Save Preference
            $status = true;
            try {
                $preference->save();
            } catch (Exception $e) {
                $status = false;
            }//end try

            //Response Preference Mercado Pago
            $data               = [];
            $data['status']     = $status;
            if($status == true){
                $data["init_point"] = $preference->init_point;
            }//end if

            $json              = json_encode($data);
            echo $json;
            die();
        }elseif($searchProfile["code"] == 400){
            $data           = [];
            $data["status"] = false;
            $data["code"]   = $searchProfile["code"];
            $div = '<div class="row">
                        <div class="col-12">
                            <div class="alert alert-info">
                                <h4>Hola! '.$searchProfile["profile"]["name"].' <br> <small>('.$searchProfile["profile"]["email"].')</small></h4> <br>
                                <h5>Ya tienes un pago aprobado para este curso, para ingresar haz clic <a href="'.Url::to(['site/login']).'" target="_blank">aquí</a></h5>
                            </div>
                        </div>
                    </div>';
            $data["div"]   = $div;
            $json           = json_encode($data);
            echo $json;
            die();
        }elseif($searchProfile["code"] == 402){
            $data           = [];
            $data["status"] = false;
            $data["code"]   = $searchProfile["code"];
             $div = '<div class="row">
                        <div class="col-12">
                            <div class="alert alert-warning">
                                <h4>Hola! '.$searchProfile["profile"]["name"].' <br> <small>('.$searchProfile["profile"]["email"].')</small></h4> <br>
                                <h5>Cuentas con un ticket de pago pendiente para este curso, puedes imprimirlo haciendo clic <a href="'.$searchProfile["urlpayment"].'" target="_blank">aquí</a> </h5><br>
                            </div>
                        </div>
                    </div>';
            $data["div"]   = $div;
            $json           = json_encode($data);
            echo $json;
            die();
        }//end if
    }//end function   

    /**
     * Save Information for Payment (Mercado Pago Respone) 
     */
    public function actionPago(){
        $response      = Yii::$app->request->get();

        //Decode payment response 
        $collection_id       = !isset($response["collection_id"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["collection_id"];
        $collection_status   = !isset($response["collection_status"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["collection_status"];
        $payment_id          = !isset($response["payment_id"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["payment_id"];
        $status              = !isset($response["status"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["status"];
        $external_reference  = !isset($response["external_reference"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["external_reference"];
        $merchant_order_id   = !isset($response["merchant_order_id"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["merchant_order_id"];
        $preference_id       = !isset($response["preference_id"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["preference_id"];
        $site_id             = !isset($response["site_id"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["site_id"];
        $processing_mode     = !isset($response["processing_mode"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["processing_mode"];
        $merchant_account_id = !isset($response["merchant_account_id"]) ? new NotFoundHttpException('La información del pago no es correcta o esta incompleta.') : $response["merchant_account_id"];;

        //Mercado Pago Access Token Seller
        $access_token = Yii::$app->params["mercadopago"]["dev"]["seller"]["access_token"];

        //Search Payment
        $mp     = new SDK();
        $mp->initialize();
        $config = $mp->config();
        $config->set('ACCESS_TOKEN', $access_token);

        $payment_info = $mp->get('/v1/payments/'.$collection_id);
        $payment_code = $payment_info['code'];

        //Success Transaction
        if($payment_code == 200 || $payment_code == 201){
            /**
             * 
             * Search Payment information for not duplicate
             * 
            */
            $searchPayment = Orderpayment::findOne(["orderTransactionId"=>$collection_id]);
            if(is_null($searchPayment)){
                //Payment Info
                $date_created          = $payment_info['body']['date_created'];
                $date_approved         = $payment_info['body']['date_approved'];
                $date_last_updated     = $payment_info['body']['date_last_updated'];
                $date_of_expiration    = $payment_info['body']['date_of_expiration'];
                $authorization_code    = $payment_info['body']['authorization_code'];
                $description           = $payment_info['body']['description'];
                $external_reference    = $payment_info['body']['external_reference'];
                $payment_method_id     = $payment_info['body']['payment_method_id'];
                $payment_type_id       = $payment_info['body']['payment_type_id'];
                $status                = $payment_info['body']['status'];
                $status_detail         = $payment_info['body']['status_detail'];
                $external_resource_url = $payment_info['body']['transaction_details']['external_resource_url']; //Ticket
                $total_paid_amount     = $payment_info['body']['transaction_details']['total_paid_amount']; 
                if(isset($payment_info['body']['transaction_details']['verification_code'])){
                    $verification_code     = $payment_info['body']['transaction_details']['verification_code']; //Ticket
                }else{
                    $verification_code = null;
                }//end if


                //Profile Info
                $profile_email      = $payment_info['body']['payer']['email'];
                $profile_first_name = !isset($payment_info['body']['additional_info']['payer']['first_name']) ? null : $payment_info['body']['additional_info']['payer']['first_name'];
                $profile_last_name  = !isset($payment_info['body']['additional_info']['payer']['last_name']) ? null : $payment_info['body']['additional_info']['payer']['last_name'];
                $profile_phone      = !isset($payment_info['body']['additional_info']['payer']['phone']['number']) ? null : $payment_info['body']['additional_info']['payer']['phone']['number'];

                //Product Info
                $product_id          = $payment_info['body']['additional_info']['items'][0]['id'];
                $product_name        = $payment_info['body']['additional_info']['items'][0]['title'];
                $product_price       = $payment_info['body']['additional_info']['items'][0]['unit_price'];
                $product_description = $payment_info['body']['additional_info']['items'][0]['description'];
                $product_quantity    = $payment_info['body']['additional_info']['items'][0]['quantity'];
                //Search Course by code
                $modelCourse = Course::findOne(['code'=>$product_id]);


                /**
                * Save Client Info in Profile Table
                */
                //Search Profile for not duplicate
                $profileModel = Profile::findOne(['email'=>$profile_email]);
                if(!is_null($profileModel)){
                    $profileModel->firstname = $profile_first_name;
                    $profileModel->lastname  = $profile_last_name;
                    $profileModel->phone     = $profile_phone;
                    $profileModel->save(false);
                }else{
                    $profileModel            = new Profile();
                    $profileModel->firstname = $profile_first_name;
                    $profileModel->lastname  = $profile_last_name;
                    $profileModel->email     = $profile_email;
                    $profileModel->phone     = $profile_phone;
                    $profileModel->save(false);
                }//end if

                /**
                 * Save Order
                 */
                $orderModel                 = new Order();
                $orderModel->profile_id     = $profileModel->profile_id;
                $orderModel->payment_method = $payment_method_id;
                $orderModel->status         = $status;
                $orderModel->creation_date  = !empty($date_created) ? date("Y-m-d H:i:s",strtotime($date_created)) : null;
                $orderModel->payment_type   = $payment_type_id;
                $orderModel->save(false);

                /**
                 * Save Order item
                 */
                $orderItemModel              = new Orderitem();
                $orderItemModel->order_id    = $orderModel->order_id;
                $orderItemModel->product_id  = !isset($modelCourse->course_id) ? null : $modelCourse->course_id;
                $orderItemModel->typeproduct = "curso";
                $orderItemModel->sku         = $product_id;
                $orderItemModel->name        = $product_name;
                $orderItemModel->price       = $product_price;
                $orderItemModel->quantity    = $product_quantity;
                $orderItemModel->save(false);

                /**
                 * Save Order Payment
                 */
                $orderPayment                     = new Orderpayment();
                $orderPayment->order_id           = $orderModel->order_id;
                $orderPayment->code               = $payment_code;
                $orderPayment->orderTransactionId = $payment_id;
                $orderPayment->state              = $status;
                $orderPayment->trazabilityCode    = $payment_method_id."|".$payment_type_id;
                $orderPayment->authorizationCode  = $authorization_code;
                $orderPayment->responseCode       = $status_detail;
                $orderPayment->operationDate      = !empty($date_created) ? date("Y-m-d H:i:s",strtotime($date_created)) : null;
                $orderPayment->updateDate         = date("Y-m-d H:i:s",strtotime($date_last_updated));;
                $orderPayment->approvedDate       = !empty($date_approved) ? date("Y-m-d H:i:s",strtotime($date_approved)) : null;
                $orderPayment->pendingreason      = $preference_id;
                $orderPayment->expirationdate     = !empty($date_of_expiration) ? date("Y-m-d H:i:s",strtotime($date_of_expiration)) : null;
                $orderPayment->reference          = $external_reference;
                $orderPayment->barcode            = $verification_code;
                $orderPayment->urlpayment         = $external_resource_url;
                $orderPayment->totalpayment       = $total_paid_amount;
                $orderPayment->save(false);
            }else{
                $orderPayment = $searchPayment;
            }//end if

            /**
             * Payment State
             */
            $state   = $orderPayment->state;
            $profile = $orderPayment->order->profile;
            $items   = $orderPayment->order->orderitems;

            if($state == "approved"){
                //Create user and send email to login
                $response_user = $this->createUpdateUserbyPayment($profile,$state,$items);
                if($response_user["status"] == true){
                    if($response_user["action"] == "insert"){
                        die("¡Felicidades! Su pago fue aprobado y el curso '".$items[0]->name."' ya se encuentra habilitado en su cuenta");    
                    }elseif($response_user["action"] == "update"){
                        die("¡Felicidades! Su pago fue aprobado y el curso '".$items[0]->name."' ya fue agregado a su cuenta");    
                    }//end if
                }elseif($response_user["status"] == false){
                    die("Su pago ya fue registrado con anterioridad y el curso '".$items[0]->name."' ya se encuentra habilitado en su cuenta");
                }//end if
            }elseif($state == "pending"){
                die("Pago en espera de confirmación");
            }elseif($state == "rejected"){
                die("Pago rechazado");
            }//end if

        }elseif ($payment_code == 400 || $payment_code == 404) {
            die("No se pudo procesar el pago");
        }//end if
    }//end function

    /**
     * Function to create user if paymente is approved
     */
    public function createUpdateUserbyPayment($profile = null,$state = null,$items = null){
        //Search user model
        $userModel = User::findOne(['email'=>$profile->email]);

        //Register new user
        if(is_null($userModel)){
            $username              = current(explode("@",$profile->email));//Get username
            $password              = User::generatePasswordRandom(); //Generate password random

            $modelSigup            = new SignupForm();
            $modelSigup->username  = $username;
            $modelSigup->firstName = $profile->firstname;
            $modelSigup->lastName  = $profile->lastname;
            $modelSigup->email     = $profile->email;
            $modelSigup->password  = $password;
            //Save info and send email with user information
            $saveUser = $modelSigup->createnewUser($items);

            //Save and send email success
            if($saveUser == true){
                 $return = [
                    "status"   => true,
                    "action"   => "insert"
                ];
            }else{//Save and send email fail
                $msg = "No se pudo registrar su usuario, por favor póngase en contacto con el administrador del sitio. \n";
                $msg.= "Para darle seguimiento porfavor envíe sus datos a hola@beautysmart.com con el siguiente id de pago : ".$_GET["collection_id"];
                throw new BadRequestHttpException($msg);
            }//end if
        }else{
            //Update register user
            //Add course to user profile
            $user_courses  = explode(',', $userModel->courses);
            $items_courses = [];
            $flag_update   = false;
            foreach ($items as $item) {
                if(!in_array($item->product_id, $user_courses)){
                    array_push($user_courses,$item->product_id);
                    $flag_update   = true;
                }//end if
            }//end foreach

            if($flag_update == true){
                $courses            = implode(',',$user_courses);
                $userModel->courses = $courses;
                $userModel->save();

                $return = [
                    "status"   => true,
                    "action"   => "update",
                    "user_id"  => $userModel->id
                ];
            }else{
                $return = [
                    "status"   => false,
                    "action"   => "any",
                    "user_id"  => $userModel->id
                ];
            }//end if
        }//end if

        return $return;
    }//end function

    /**
     * Function to Confirm user and email generated
     */
    public function actionConfirmaremail($token){
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }//end try

        //Change status user (Active)
        if ($user = $model->verifyEmail()) {
            Yii::$app->session->setFlash('success', 'Su correo electrónico ha sido confirmado, ahora pude ingresar a su cuenta.');
            return $this->goHome();
        }//end if
    }//end function
}