<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['cursos/confirmaremail', 'token' => $user->verification_token]);
?>
<div class="verify-email">
    <p>Hola <?= Html::encode($user->firstName." ".$user->lastName) ?>,</p>
    <p>
        Te informamos que tu cuenta en Beauty Smart se generó correctamente, solo resta verificar tu corre electrónico. <br>
        Una vez verifcado podrás ingresar con los siguientes datos: <br>
        <ul>
            <li>
                Nombre de usuario : <strong><?=Html::encode($user->username);?></strong>
            </li>
            <li>
                Password : <strong><?=Html::encode($pass);?></strong>
            </li>
        </ul>
    </p>

    <p>Por favor usa el siguiente link para activar tu cuenta y poder ingresar al portal de cursos :</p>
    <p><?= Html::a(Html::encode($verifyLink), $verifyLink) ?></p>
    <p>
        Atte: <?=Html::encode(Yii::$app->name);?>
    </p>
</div>
