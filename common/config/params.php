<?php
return [
    'adminEmail'                    => 'ahr.mpdev@gmail.com',
    'supportEmail'                  => 'ahr.mpdev@gmail.com',
    //'senderEmail'                   => 'noreply@beautysmartdev.endinahosting.com',
    'senderEmail'                   => 'arturo@vicom.mx',
    'senderName'                    => 'Beauty Smart | Transformando la belleza',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength'        => 8,
];
