<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%contact}}".
 *
 * @property int $contact_id
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property string $body
 * @property string $created_at
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%contact}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'subject', 'body', 'created_at'], 'required'],
            [['body'], 'string'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 150],
            [['subject'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contact_id' => 'Contact ID',
            'name'       => 'Nombre',
            'email'      => 'Correo electrónico',
            'subject'    => 'Asunto',
            'body'       => 'Comentarios',
            'created_at' => 'Created At',
        ];
    }
}
