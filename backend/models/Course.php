<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%course}}".
 *
 * @property int $course_id
 * @property int $category_id
 * @property string $title
 * @property string|null $introduction
 * @property string|null $author
 * @property string $description
 * @property string $created_at
 * @property string|null $requirements
 * @property float|null $hours
 * @property int|null $articles
 * @property float $price
 * @property int|null $discount
 *
 * @property Category $category
 * @property Instructor $instructor
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%course}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'title', 'price','estatus','instructor_id','introduction','materials','topics','videointro'], 'required'],
            [['category_id', 'articles', 'discount','instructor_id'], 'integer'],
            [['description', 'requirements','estatus'], 'string'],
            [['created_at'], 'safe'],
            [['hours', 'price'], 'number'],
            [['title', 'introduction', 'author','mainimage','secondaryimage','thirdimage','bannerimage'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'category_id']],
            [['instructor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instructor::className(), 'targetAttribute' => ['instructor_id' => 'instructor_id']],

            ['code', 'trim'],
            ['code','required'],
            ['code','string' , 'max'=>12],
            ['code', 'unique', 'targetClass' => '\backend\models\Course', 'message' => 'Existe.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'course_id'      => 'ID',
            'category_id'    => 'Categoría',
            'title'          => 'Tipo del curso',
            'introduction'   => 'Introducción',
            'author'         => 'Instructor',
            'description'    => 'Descripción del curso',
            'created_at'     => 'Fecha inicio del curso',
            'requirements'   => 'Requisitos',
            'hours'          => 'Horas de duración del curso',
            'articles'       => 'Articulos',
            'price'          => 'Precio',
            'discount'       => 'Descuento',
            'mainimage'      => 'Imagen Principal',
            'secondaryimage' => 'Imagen Secundaria',
            'thirdimage'     => 'Otra imagen',
            'bannerimage'    => 'Imagen Banner',
            'estatus'        => 'Estatus',
            'instructor_id'  => 'Instructor',
            'materials'      => 'Materiales',
            'topics'         => 'Temario',
            'videointro'     => 'Video Presentación (Url)',
            'code'           => 'Código del curso',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }

    /**
     * Gets query for [[Instructor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstructor()
    {
        return $this->hasOne(Instructor::className(), ['instructor_id' => 'instructor_id']);
    }
}
