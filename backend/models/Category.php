<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $category_id
 * @property int|null $categoryparent_id
 * @property string $name
 * @property string|null $description
 * @property string|null $estatus
 *
 * @property Category $categoryparent
 * @property Category[] $categories
 */
class Category extends \yii\db\ActiveRecord
{
    public $sliderImages;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoryparent_id'], 'integer'],
            [['name','estatus'], 'required'],
            [['description', 'estatus', 'mainimage'], 'string'],
            [['name'], 'string', 'max' => 180],
            [['sliderImages'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 352,'maxWidth'=>352,'minHeight'=>528,'maxHeight'=>528,'maxSize'=>1024 * 1024 * 2],
            [['categoryparent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryparent_id' => 'category_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id'       => 'ID',
            'categoryparent_id' => 'Categoría Padre',
            'name'              => 'Nombre',
            'description'       => 'Descripción',
            'mainimage'         => 'Imagen Principal',
            'sliderImages'      => 'Imagen Carrusel',
            'estatus'           => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Categoryparent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryparent()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'categoryparent_id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['categoryparent_id' => 'category_id']);
    }

    /**
     * Gets query for [[Courses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['category_id' => 'category_id'])->andOnCondition(['estatus'=>'active']);
    }
}
