<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $product_id
 * @property string $sku
 * @property string $title
 * @property string $description
 * @property string|null $mainimage
 * @property string|null $estatus
 */
class Product extends \yii\db\ActiveRecord
{
    public $imagen;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sku', 'title', 'description','estatus'], 'required'],
            [['description', 'estatus'], 'string'],
            [['sku', 'title', 'mainimage'], 'string', 'max' => 255],
            [['sku'], 'unique'],

            [['imagen'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 190,'maxWidth'=>1000,'minHeight'=>190,'maxHeight'=>1000,'maxSize'=>1024 * 1024 * 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id'  => 'Product ID',
            'sku'         => 'Sku',
            'title'       => 'Nombre del producto',
            'description' => 'Descripción',
            'mainimage'   => 'Imagen',
            'estatus'     => 'Estatus',
        ];
    }
}
