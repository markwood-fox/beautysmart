<?php

namespace backend\controllers;

use Yii;
use backend\models\Category;
use backend\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class'=> AccessControl::className(),
                'only' => ['index','create','update','delete'],
                'rules' => [
                    [
                        'allow' =>true,
                        'roles' => ['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Get all parent category for select input in form [create & update]
     */
    public function getparentsCategory(){
        $model = new Category;
        return $model->find()->where(['categoryparent_id' => null])->all();
    }


    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model           = new Category();
        $parentsCategory = $this->getparentsCategory();

        if ($model->load(Yii::$app->request->post())) {

            //image slider home
            $model->sliderImages = UploadedFile::getInstance($model,'sliderImages');
            if(!empty($model->sliderImages)){
                $imageName             = $model->sliderImages->name;
                $model->sliderImages->saveAs('uploads/category/'.$imageName);
                $model->mainimage = 'uploads/category/'.$imageName;
            }else{
                $model->mainimage = null;
            }//end if

            $model->save(false);

            Yii::$app->session->setFlash('success', "Se registro correctamente la categoría :  <strong>".$model->name."</strong>");
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->category_id]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'parentsCategory' => $parentsCategory
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model           = $this->findModel($id);
        $parentsCategory = $this->getparentsCategory();

        if ($model->load(Yii::$app->request->post())) {
            //image slider home
            $model->sliderImages = UploadedFile::getInstance($model,'sliderImages');
            if(!empty($model->sliderImages)){
                $imageName             = $model->sliderImages->name;
                $model->sliderImages->saveAs('uploads/category/'.$imageName);
                $model->mainimage = 'uploads/category/'.$imageName;
            }//end if

            $model->save(false);


            Yii::$app->session->setFlash('success', "Se actualizo correctamente la categoría :  <strong>".$model->name."</strong>");
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->category_id]);
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'parentsCategory' => $parentsCategory
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
