<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Course */
$tmp = $model->title == 'basico' ? 'Básico' : 'Avanzado';
//$this->title = $model->category->name." (".$tmp.")";
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-danger">
            <div class="card-header">
                <h1 class="card-title"><strong>&nbsp;&nbsp;&nbsp;<?=Html::encode($model->category->name." (".$tmp.")") ?></strong></h1>
            </div>
            <div class="contact-view card-body">
                <div class="col-12" align="right">
                    <?= Html::button('<i class="fas fa-edit"></i>', ['value'=>Url::to(['update','id' => $model->course_id]),'class' => 'btn bg-teal btn-sm btnUpdateView', 'title'=>'Editar']) ?>
                    <?= Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'id' => $model->course_id], [
                            'class' => 'btn btn-danger btn-sm',
                            'data' => [
                                'confirm' => '¿Está seguro de eliminar este elemento?',
                                'method' => 'post',
                            ],
                        ]) ?>
                </div>
                <div class="col-12 pt-3">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'course_id',
                            'code',
                            [
                                'label'     => 'Categoría',
                                'attribute' => 'category_id',
                                'format'    => 'html',
                                'value'     =>function($model){
                                    return $model->category->name;
                                }
                            ],
                            [
                                'label'     => 'Tipo del curso',
                                'attribute' => 'title',
                                //'format'    => 'html',
                                'value' => function($model){
                                    if($model->title == "basico"){
                                        return "Básico";
                                    }else{
                                        return "Avanzado";
                                    }//end if
                                }//
                            ],
                            [
                                'label'     => 'Instructor',
                                'attribute' => 'instructor_id',
                                'value' => function($model){
                                    return $model->instructor->firstname." ".$model->instructor->lastname;
                                }//
                            ],
                            'introduction',
                            'topics:ntext',
                            'materials:ntext',
                            //'author',
                            'description:ntext',
                            'requirements:ntext',
                            'created_at',
                            'hours',
                            //'articles',
                            'price',
                            [
                                'label' => 'Estatus',
                                'attribute' => 'estatus',
                                'format' => 'html',
                                'contentOptions' => ['align'=> 'center'],
                                'value' => function($model){
                                    if($model->estatus == "active"){
                                        return "<div class='col-4 alert-success'>Activo</div>";
                                    }else{
                                        return "<div class='col-4 alert-danger'>Inactivo</div>";
                                    }//end if
                                }
                            ],
                            //'discount',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="card-footer text-center">
                <?=  Html::button('Cerrar',['value'=>'','class'=>'btn btn-sm btn-success cancelView', 'title'=>'Cerrer']) ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /*** Action Button Edit View ***/
    $(".btnUpdateView").on("click",function(e){
        $("#divEditForm").hide(function(e){});
        $("#btnAddForm").show(function(e){});
        $("#divEditForm").load($(this).attr('value'),function(e){
            $("#divEditForm").slideDown('slow');
        });
    });

    /*** Action Button Cancel-Close View ***/
    $(".cancelView").on("click",function(e){
         $("#divEditForm").slideUp(function(e){});
    });
</script>

