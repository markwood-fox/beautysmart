<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instructor */

$this->title = 'Actualizar información de: ' . $model->firstname." ".$model->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Instructors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->instructor_id, 'url' => ['view', 'id' => $model->instructor_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="instructor-update">
    <div class="row-fluid">
        <div class="col-sm-12">
            <div class="card card-danger">
                <div class="card-header">
                    <h1 class="card-title"><strong><i class="nav-icon fas fa-edit"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
                    <button type="button" class="btn close text-white" onclick='closeForm("instructorForm")'>×</button>
                </div>
                <?=$this->render('_form', [
                    'model' => $model
                ]) ?>
            </div>
            <!--.card-->
        </div>    
    </div>
</div>