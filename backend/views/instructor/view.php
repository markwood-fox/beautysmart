<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Instructor */

$this->title = $model->firstname." ".$model->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Instructor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox-view',
]);
?>
<div class="row-fluid">
    <div class="col-xs-12">
        <div class="card card-danger">
            <div class="card-header">
                <h1 class="card-title"><strong>&nbsp;&nbsp;&nbsp;<?=Html::encode($this->title) ?></strong></h1>
            </div>
            <div class="contact-view card-body">
                <div class="col-12" align="right">
                    <?= Html::button('<i class="fas fa-edit"></i>', ['value'=>Url::to(['update','id' => $model->instructor_id]),'class' => 'btn bg-teal btn-sm btnUpdateView', 'title'=>'Editar']) ?>
                    <?= Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'id' => $model->instructor_id], [
                            'class' => 'btn btn-danger btn-sm',
                            'data' => [
                                'confirm' => '¿Está seguro de eliminar este elemento?',
                                'method' => 'post',
                            ],
                        ]) ?>
                </div>
                <div class="col-12 pt-3">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'instructor_id',
                            'firstname',
                            'lastname',
                            'trajectory:ntext',
                            [
                                'label' => 'Foto',
                                'attribute' => 'perfilimage',
                                'format' => 'html',
                                'contentOptions' => ['align'=> 'center'],
                                'value'     =>function($model){
                                    return Html::a(Html::img(Url::base()."/".$model->perfilimage,['height'=>'100']),Url::base()."/".$model->perfilimage,['title'=>'Ver Imagen','class' => 'data-fancybox-view']);
                                }

                            ],
                            [
                                'label' => 'Imagen 1',
                                'attribute' => 'image1',
                                'format' => 'html',
                                'contentOptions' => ['align'=> 'center'],
                                'value' => function($model){
                                    if(empty($model->image1)){
                                        return "Sin imagen";
                                    }else{
                                        return Html::a(Html::img(Url::base()."/".$model->image1,['height'=>'100']),Url::base()."/".$model->image1,['title'=>'Ver Imagen','class' => 'data-fancybox-view']); 
                                    }//end if
                                }
                            ],
                            [
                                'label' => 'Imagen 2',
                                'attribute' => 'image2',
                                'format' => 'html',
                                'contentOptions' => ['align'=> 'center'],
                                'value' => function($model){
                                    if(empty($model->image2)){
                                        return "Sin imagen";
                                    }else{
                                        return Html::a(Html::img(Url::base()."/".$model->image2,['height'=>'100']),Url::base()."/".$model->image2,['title'=>'Ver Imagen','class' => 'data-fancybox-view']); 
                                    }//end if
                                }
                            ],
                            [
                                'label' => 'Imagen 3',
                                'attribute' => 'image3',
                                'format' => 'html',
                                'contentOptions' => ['align'=> 'center'],
                                'value' => function($model){
                                    if(empty($model->image2)){
                                        return "Sin imagen";
                                    }else{
                                        return Html::a(Html::img(Url::base()."/".$model->image3,['height'=>'100']),Url::base()."/".$model->image3,['title'=>'Ver Imagen','class' => 'data-fancybox-view']); 
                                    }//end if
                                }
                            ],
                            [
                                'label' => 'Imagen 4',
                                'attribute' => 'image4',
                                'format' => 'html',
                                'contentOptions' => ['align'=> 'center'],
                                'value' => function($model){
                                    if(empty($model->image3)){
                                        return "Sin imagen";
                                    }else{
                                        return Html::a(Html::img(Url::base()."/".$model->image4,['height'=>'100']),Url::base()."/".$model->image4,['title'=>'Ver Imagen','class' => 'data-fancybox-view']); 
                                    }//end if
                                }
                            ],
                            'fblink',
                            'iglink',
                            'wplink',
                            'twlink',
                            [
                                'label' => 'Estatus',
                                'attribute' => 'estatus',
                                'format' => 'html',
                                'contentOptions' => ['align'=> 'center'],
                                'value' => function($model){
                                    if($model->estatus == "active"){
                                        return "<div class='col-4 alert-success'>Activo</div>";
                                    }else{
                                        return "<div class='col-4 alert-danger'>Inactivo</div>";
                                    }//end if
                                }
                            ]
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="card-footer text-center">
                <?=  Html::button('Cerrar',['value'=>'','class'=>'btn btn-sm btn-success cancelView', 'title'=>'Cerrer']) ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /*** Action Button Edit View ***/
    $(".btnUpdateView").on("click",function(e){
        $("#divEditForm").hide(function(e){});
        $("#btnAddForm").show(function(e){});
        $("#divEditForm").load($(this).attr('value'),function(e){
            $("#divEditForm").slideDown('slow');
        });
    });

    /*** Action Button Cancel-Close View ***/
    $(".cancelView").on("click",function(e){
         $("#divEditForm").slideUp(function(e){});
    });
</script>

