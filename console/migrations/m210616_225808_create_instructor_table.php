<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%instructor}}`.
 */
class m210616_225808_create_instructor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%instructor}}', [
            'instructor_id' => $this->primaryKey(),
            'firstname'     => $this->string()->notNull(),
            'lastname'      => $this->string()->notNull(),
            'trajectory'    => $this->text()->notNull(),
            'perfilimage' => $this->string(),
            'image1'        => $this->string(),
            'image2'        => $this->string(),
            'image3'        => $this->string(),
            'image4'        => $this->string(),
            'fblink'        => $this->string(),
            'iglink'        => $this->string(),
            'wplink'        => $this->string(),
            'twlink'        => $this->string(),
            'estatus' => "ENUM('active','inactive')"
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%instructor}}');
    }
}
