<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course}}`.
 */
class m210528_231038_create_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course}}', [
            'course_id'    => $this->primaryKey(),
            'category_id'  => $this->integer()->notNull(),
            'title'        => $this->string()->notNull(),
            'introduction' => $this->string(),
            'author'       => $this->string(),
            'description'  => $this->text()->notNull(),
            'created_at'   => $this->dateTime()->notNull(),
            'requirements' => $this->text(),
            'hours'        => $this->float(),
            'articles'     => $this->integer(),
            'price'        => $this->money(8,2)->notNull(),
            'discount'     => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-course-category_id',
            'course',
            'category_id',
            'category',
            'category_id',
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-course-category_id',
            'course'
        );
        $this->dropTable('{{%course}}');
    }
}
