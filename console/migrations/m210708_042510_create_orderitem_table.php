<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orderitem}}`.
 */
class m210708_042510_create_orderitem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orderitem}}', [
            'order_id'    => $this->primaryKey(),
            'product_id'  => $this->integer(),
            'order_id'    => $this->integer()->notNull(),
            'typeproduct' => $this->string(15),
            'sku'         => $this->string(),
            'name'        => $this->string(),
            'price'       => $this->money(8,2),
            'quantity'    => $this->integer(10),
        ]);

        $this->addForeignKey(
            'fk-orderitem-order_id',
            'orderitem',
            'order_id',
            'order',
            'order_id',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-order_id',
            'orderitem'
        );
        $this->dropTable('{{%orderitem}}');
    }
}
