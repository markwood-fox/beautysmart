<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profile}}`.
 */
class m210708_035046_create_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profile}}', [
            'profile_id' => $this->primaryKey(),
            'firstname'  => $this->string()->notNull(),
            'lastname'   => $this->string()->notNull(),
            'email'      => $this->string()->notNull(),
            'phone'      => $this->integer(20)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile}}');
    }
}
