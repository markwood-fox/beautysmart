<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m210527_224802_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'category_id' => $this->primaryKey(),
            'categoryparent_id' => $this->integer(),
            'name' => $this->string(180)->notNull(),
            'description' => $this->text(),
            'estatus' => "ENUM('active','inactive')"
        ]);

        $this->addForeignKey(
            'fk_category_parentcategory',
            '{{%category}}',
            'categoryparent_id',
            '{{%category}}',
            'category_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category}}');
        $this->dropForeignKey('fk_category_parentcategory','{{$category}}');
    }
}
