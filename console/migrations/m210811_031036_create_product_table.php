<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m210811_031036_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'product_id'  => $this->primaryKey(),
            'sku'         => $this->string()->notNull()->unique(),
            'title'       => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'mainimage'   => $this->string(),
            'estatus'     => "ENUM('active','inactive')",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
