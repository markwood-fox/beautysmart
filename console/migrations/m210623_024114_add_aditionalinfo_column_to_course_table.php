<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%course}}`.
 */
class m210623_024114_add_aditionalinfo_column_to_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%course}}', 'materials', $this->text()->notNull());
        $this->addColumn('{{%course}}', 'topics', $this->text()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%course}}', 'materials');
        $this->dropColumn('{{%course}}', 'topics');
    }
}
